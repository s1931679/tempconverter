package com.example.tempconverter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConverter {

    @Test
    public void testBook1() throws Exception {
        Converter converter = new Converter();
        double temp = converter.convert(0.0);
        Assertions.assertEquals(32.0, temp, 0.0, "Price of a book 1");
    }

}
