package com.example.tempconverter;

/** Converts C to F */
public class Converter {

    public double convert(double temperature) {
        return temperature * 1.8 + 32;
    }

}
