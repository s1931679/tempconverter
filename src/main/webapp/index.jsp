<!DOCTYPE HTML>
<!-- Front end for the webBookQuote Servlet. -->

<html lang="en">
<head>
    <title>Temp Converter Application</title>
    <link rel="stylesheet" href="styles.css">
    <meta charset="UTF-8">
</head>

<body>
<h1>Temp Converter Application</h1>

<form ACTION="./tempConverter">
    <p>Enter temperature in C:  <input TYPE="NUMBER" NAME="temp"></p>
    <input TYPE="SUBMIT">
</form>

</body>
</html>
